package com.cgi.boxing;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

	private List<String> temp = Arrays.asList("t1", "t2"); // immutable -- List.of("t1","t2");
	private List<String> names = new ArrayList<>(8);

	public static void main(String[] args) {

		long currTimestamp = System.currentTimeMillis(); // UTC

		System.out.println(currTimestamp);
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

		System.out.println(date);
		//
//		double d1 = 5.53249;
//		double d2 = 5.413423;
//
//		BigDecimal bigDecimal = BigDecimal.valueOf(d1);
//		BigDecimal bigDecimal2 = BigDecimal.valueOf(d2);
//
//		System.out.println(d1 + d2);
//		System.out.println(bigDecimal.add(bigDecimal2));
	}
}
