package com.cgi.io;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ReadFile {

	public String readFile(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = "";
			String content = "";
			while ((line = br.readLine()) != null) {
				content = content + line;
			}
			return content;
		} catch (IOException io) {
			//
		}
		return null;
	}

	// zapis BufferedWriter a FileWriter... a metoda write

	public List<String> readFile(Path path) throws IOException {
		return Files.readAllLines(path, Charset.defaultCharset());
	}

}
