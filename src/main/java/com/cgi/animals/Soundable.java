package com.cgi.animals;

public interface Soundable {

	void sound();

}
