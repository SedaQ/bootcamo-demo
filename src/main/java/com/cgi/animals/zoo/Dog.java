package com.cgi.animals.zoo;

import com.cgi.animals.Animal;

public class Dog extends Animal {

	public Dog(String title, String description) {
		super(title, description);
	}

	@Override
	public void sound() {
		System.out.println("Haf!");
	}
	

	@Override
	public String toString() {
		return "Dog [getTitle()=" + getTitle() + ", getDescription()=" + getDescription() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
