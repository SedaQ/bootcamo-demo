package com.cgi.animals.zoo;

import com.cgi.animals.Animal;

public class Cat extends Animal {

	public Cat(String title, String description) {
		super(title, description);
	}

	@Override
	public void sound() {
		System.out.println("Mnau!");
	}

}
