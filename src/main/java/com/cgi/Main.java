package com.cgi;

import java.io.File;
import java.util.*;

import com.cgi.generics.PageResult;

public class Main {

	public static void main(String[] args) {
		List<String> names = new ArrayList<>();
		names.add("test");

		PageResult<Test1> pageResult = new PageResult<>();

		Test1 myTest1 = Test1.now("test1");

		Test1 myTest2 = Test1.reverseText("test1");

		System.out.println("Oko je palindrom?: " + Main.isPalindrom("oko"));
		System.out.println("Pes je palindrom?: " + Main.isPalindrom("pes"));

		String ss = "p1" + "p2" + "p3" + "p4" + "p5" + "p6";

		String ss2 = new StringBuilder("p1").append("p2").append("p3").toString();

		String s3 = 6 + 3 + "2";
		String s4 = "6" + (3 + 2);

		int age = 54;
		Integer age2 = 54;

		System.out.println(s3); // 92
		System.out.println(s4); // 632

		File f = new File("..");
		//

		Main.isEquals("test1", "test2");
	}

	public static boolean isPalindrom(String originalText) {
		String reversedString = new StringBuilder(originalText).reverse().toString();
		return reversedString.equals(originalText);
	}

	public static <T> T isEquals(T t1, T t2) {
		return t2;
	}
}
