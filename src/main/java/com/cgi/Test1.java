package com.cgi;

public class Test1 {

	private String text;

	private Test1() {
		// ..
	}

	private Test1(String test1) {
		this.text = test1;
		//
	}

	public static Test1 now(String test1) {
		return new Test1(test1);
	}

	public static Test1 reverseText(String test1) {
		String reversed = new StringBuilder(test1).reverse().toString();
		return new Test1(reversed);
	}
}
