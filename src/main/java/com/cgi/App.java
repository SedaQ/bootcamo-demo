package com.cgi;

import java.io.File;
import java.util.*;

import com.cgi.animals.*;
import com.cgi.animals.zoo.*;

public class App {

	// daemon thread - GC
	public static void main(String[] args) {

		List<Animal> animals = new ArrayList<>();
		animals.add(new Dog("Dog1", "Super dog!"));
		animals.add(new Cat("Cat1", "Super dog!"));

		for (Animal animal : animals) {
			System.out.println(animal);
		}

		File bootcampUsers = new File("C:/Users/SedaQ/..");
		readBootcampUsers(bootcampUsers);

		int age = 5;
		String pwd = new String("helloworld"); // (y)
		String pwd2 = "helloworld"; // (y)
		pwd2 = null;
		char[] pwd3 = { 'h', 'e', 'l', 'l', 'o', 'w', 'o', 'r', 'l', 'd' };

		String line = "Hello world" + System.getProperty("line.separator") + "Hello world, the next day.";

		// Proc se casto u hesel pouziva char[]
		// knihovna Lombok ... pro generovani -- konstruktoru, hashCode/equals,
		// getteru/setteru, toStringu,...
		// v Jave 15/16 record...

		// List, Set, Mapa

		ArrayDeque<Animal> ar = new ArrayDeque<>();
		List<Animal> animalsList = new ArrayList<>();
		Set<Animal> animalsSet = new HashSet<>();

		// red black trees

	}

	public static void readBootcampUsers(File file) {

	}
}
